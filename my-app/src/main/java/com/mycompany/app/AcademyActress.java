package com.mycompany.app;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class AcademyActress {
  private String year;
  private String name;
  private String movie;

  public AcademyActress(String year, String name, String movie) {
    this.year = year;
    this.name = name;
    this.movie = movie;
  }

  public static void main(String[] args) {
    Pattern pattern = Pattern.compile(",");
    String csvFile = "";
    try {
      csvFile = args[0];
    } catch (Exception e) {
      System.out.println("Please provide file name");
      System.exit(0);
    }
    try(BufferedReader br = new BufferedReader(new FileReader(csvFile));) {
      List<AcademyActress> academyActresses = br
        .lines()
        .skip(1)
        .map(line -> {
          String[] x = pattern.split(line);
          return new AcademyActress(x[0], x[1], x[2]);
        })
        .collect(Collectors.toList());
    ObjectMapper mapper = new ObjectMapper();
    mapper.enable(SerializationFeature.INDENT_OUTPUT);
    mapper.writeValue(System.out, academyActresses);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
