document.addEventListener("DOMContentLoaded", function(event) {
  // formats json object into table
  function extractActresses(data) {
    data.forEach(function(item, index) {
      var TRnode = document.createElement("TR");
      var yearNode = document.createElement("TD");
      var actressNode = document.createElement("TD");
      var movieNode = document.createElement("TD");
      var yearText = document.createTextNode(item.year);
      var actressText = document.createTextNode(item.actress);
      var movieText = document.createTextNode(item.movie);
      yearNode.appendChild(yearText);
      actressNode.appendChild(actressText);
      movieNode.appendChild(movieText);
      TRnode.appendChild(yearNode);
      TRnode.appendChild(actressNode);
      TRnode.appendChild(movieNode);
      TRnode.style.cursor = "pointer";
      // Adds and alert and redirect to youtube if row is clicked
      TRnode.addEventListener("click", function(e){
        alert(item.year + ", " + item.actress + ", " + item.movie);
        window.open("https://www.youtube.com/results?search_query=" + item.actress + "+" + item.movie);
      });
      document.getElementById('table').appendChild(TRnode);
    })
  }
  //xhr request to http://127.0.0.1:8000/academy_award_actresses.json
  function getActresses() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://127.0.0.1:8000/academy_award_actresses.json');
    xhr.responseType = "json";
    xhr.onload = function() {
      if (xhr.status === 200) {
        extractActresses(xhr.response);
      } else {
        console.log("Request failed at " + xhr.status);
      }
    }
    xhr.send();
  }
  getActresses();
});
